# Simulador MF-e

Aplicativo NodeJS para simular Entrada e Saída de arquivos referentes ao CF-e
(Cupom Fiscal Eletrônico).

A ideia é:

- Consumir os arquivos de entrada (`chokidar`)
- Decidir qual o Método foi usado
- Remover o arquivo do diretório de entrada
- Gerar um arquivo fictício e salvar no diretório de saída

## Desenvolvimento

Instalação

```bash
npm i
```

Desenvolvimento em modo debug

```bash
npm run dev --dir-entrada <dir-entrada> --dir-saida <dir-saida>
```

Execução

```bash
npm start --dir-entrada <dir-entrada> --dir-saida <dir-saida>
```

## Arquivo de configuração

O Simulador MF-e pode ser configurado através da criação de um arquivo JSON
na raiz do projeto com o nome `.simulador-mfe.json`. A estrutura segue o padrão
abaixo:

```json
{
    "emitente": {
        "cnpj": "61591208000117",
        "c_uf": 23
    },
    "modulo_fiscal": {
        "numero_serie": 900001234
    }
}
```

## Gerar versão executável

```bash
npm run bundle
```

Serão gerados executáveis em `/dist` na forma:

- `simulador-mfe-linux`
- `simulador-mfe-macos`
- `simulador-mfe-win.exe`

Para executar no Linux, por exemplo, basta digitar:

```
./simulador-mfe-linux --dir-entrada <dir-entrada> --dir-saida <dir-saida>
```
