/*
Exemplo de Saída

<Integrador>
    <Identificador>
        <Valor>10</Valor>
    </Identificador>
    <IntegradorResposta>
        <Codigo>AP</Codigo>
        <Valor>Arquivo processado</Valor>
    </IntegradorResposta>
    <Resposta>
          <CodigoAutorizacao>98800</CodigoAutorizacao>
          <Bin>123456</Bin>
          <DonoCartao>TESTE</DonoCartao>
          <DataExpiracao>01/>01</DataExpiracao>
          <InstituicaoFinanceira>STONE</InstituicaoFinanceira>
          <Parcelas>1</Parcelas>
          <UltimosQuatroDigitos>1234</UltimosQuatroDigitos>
          <CodigoPagamento>98800</CodigoPagamento>
          <ValorPagamento>15.66</ValorPagamento>
          <IdFila>1680143</IdFila>
          <Tipo>1</Tipo>
    </Resposta>
</Integrador>
*/

const debug = require('debug')
const wrapper = require('../js2xml-wrapper')
const parseParametrosEntrada = require('../parse-params-entrada')
const VfpeEnviarPagamento = require('../models/VfpeEnviarPagamento')
const VfpeVerificarStatusValidador = require('../models/VfpeVerificarStatusValidador')

const d = debug('smf:VFP-e VerificarStatusValidador')

/**
 *  Gera arquivo de Resposta para Método VerificarStatusValidador do componente VFP-e
 *  @param  {Object} entrada Objeto convertido de xml de entrada
 *  @param {Object}  chance  Instância de chance, para gerar valores randômicos
 *  @return {Promise}         filename e xml de saída
 */
module.exports = async (entrada, chance) => {
    const params = parseParametrosEntrada(entrada)
    const pagamento = await new VfpeEnviarPagamento({
        id_fila: params.IdFila
    }).fetch()

    if (!pagamento) {
        d(`Pagamento da fila ${params.IdFila} não encontrado. Gerando valor aleatório.`)
    }

    const filename = chance.word({ syllables: 5 }) + '.xml'
    const identificador = entrada.Integrador.Identificador.Valor._text
    d(`Gerando arquivo de saída para Identificador ${identificador}`)

    const id_fila = chance.string({ length: 7, pool: '0123456789'})
    const codigo_autorizacao = chance.string({ pool: '0123456789', length: 5 })
    const codigo_pagamento = chance.string({ pool: '0123456789', length: 5 })
    const tipo_bandeira = chance.integer({ min: 1, max: 9 })

    const model = await new VfpeVerificarStatusValidador({
        identificador,
        id_fila,
        codigo_autorizacao,
        codigo_pagamento,
        tipo_bandeira
    }).save()

    const expRaw = chance.exp({ raw: true })
    const exp = `${expRaw.month}/${expRaw.year.substring(2)}`

    const xml = wrapper({
        Integrador: {
            Identificador: {
                Valor: {
                    _text: identificador
                }
            },
            IntegradorResposta: {
                Codigo: {
                    _text: 'AP'
                },
                Valor: {
                    _text: 'Arquivo processado'
                }
            },
            Resposta: {
                CodigoAutorizacao: {
                    _text: codigo_autorizacao
                },
                Bin: {
                    _text: chance.string({ pool: '0123456789', length: 6 })
                },
                DonoCartao: {
                    _text: chance.name()
                },
                DataExpiracao: {
                    _text: exp
                },
                InstituicaoFinanceira: {
                    _text: chance.cc_type()
                },
                Parcelas: {
                    _text: chance.integer({ min: 1, max: 10 })
                },
                UltimosQuatroDigitos: {
                    _text: chance.string({ pool: '0123456789', length: 4 })
                },
                CodigoPagamento: {
                    _text: codigo_pagamento
                },
                ValorPagamento: {
                    _text: pagamento ? pagamento.get('valor_venda') : chance.floating({ fixed: 2, min: 10, max: 1000 })
                },
                IdFila: {
                    _text: id_fila
                },
                Tipo: {
                    _text: tipo_bandeira
                }
            }
        }
    })

    return {
        filename,
        xml
    }
}
