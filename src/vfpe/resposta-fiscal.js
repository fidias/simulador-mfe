
const debug = require('debug')
const wrapper = require('../js2xml-wrapper')

const d = debug('smf:VFP-e RespostaFiscal')

/**
 *  Gera arquivo de Resposta para Método RespostaFiscal do componente VFP-e
 *  @param  {Object}  entrada Objeto convertido de xml de entrada
 *  @param  {Object}  chance  Instância de chance, para gerar valores randômicos
 *  @return {Promise}         filename e xml de saída
 */
module.exports = async (entrada, chance) => {
    // TODO: buscar `IdFila` no sqlite e mostrar erro caso não seja encontrado.
    const filename = chance.word({ syllables: 5 }) + '.xml'
    const identificador = entrada.Integrador.Identificador.Valor._text
    d(`Gerando arquivo de saída para Identificador ${identificador}`)

    const xml = wrapper({
        Integrador: {
            Identificador: {
                Valor: {
                    _text: identificador
                }
            },
            IntegradorResposta: {
                Codigo: {
                    _text: 'AP'
                },
                Valor: {
                    _text: 'Arquivo processado'
                }
            },
            Resposta: {
                retorno: {
                    _text: chance.string({ length: 6, pool: '0123456789'})
                }
            }
        }
    })

    return {
        filename,
        xml
    }
}
