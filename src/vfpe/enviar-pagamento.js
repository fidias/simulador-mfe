/*
Exemplo de saída

<Integrador>
	<Identificador>
		<Valor>11</Valor>
	</Identificador>
	<IntegradorResposta>
		<Codigo>AP</Codigo>
		<Valor>Arquivo processado</Valor>
	</IntegradorResposta>
	<Resposta>
		<IdPagamento>1680122</IdPagamento>
		<Mensagem>Pagamento enviado com sucesso ao validador fiscal! O ID da transação para acompanhar o
status e identificá-la no portal de ajustes é 1680122</Mensagem>
		<StatusPagamento>EnviadoAoValidador</StatusPagamento>
	</Resposta>
</Integrador>
*/

const debug = require('debug')
const wrapper = require('../js2xml-wrapper')
const parseParametrosEntrada = require('../parse-params-entrada')
const VfpeEnviarPagamento = require('../models/VfpeEnviarPagamento')
const parseDecimalNumber = require("parse-decimal-number")

const d = debug('smf:VFP-e EnviarPagamento')

/**
 *  Gera arquivo de Resposta para Método EnviarPagamento do componente VFP-e
 *  @param  {Object} entrada Objeto convertido de xml de entrada
 *  @param {Object}  chance  Instância de chance, para gerar valores randômicos
 *  @return {Promise}         filename e xml de saída
 */
module.exports = async (entrada, chance) => {
    const params = parseParametrosEntrada(entrada)
    const idPagamento = chance.string({ length: 7, pool: '0123456789'})
    const filename = chance.word({ syllables: 5 }) + '.xml'
    const identificador = entrada.Integrador.Identificador.Valor._text
    d(`Gerando arquivo de saída para Identificador ${identificador}`)

    const model = await new VfpeEnviarPagamento({
        identificador,
        serial_pos: params.SerialPOS,
        valor_venda: parseDecimalNumber(params.ValorTotalVenda, '.,'),
        id_fila: idPagamento
    }).save()

    const xml = wrapper({
        Integrador: {
            Identificador: {
                Valor: {
                    _text: identificador
                }
            },
            IntegradorResposta: {
                Codigo: {
                    _text: 'AP'
                },
                Valor: {
                    _text: 'Arquivo processado'
                }
            },
            Resposta: {
                IdPagamento: {
                    _text: idPagamento
                },
                Mensagem: {
                    _text: `Pagamento enviado com sucesso ao validador fiscal! O ID da transação para acompanhar o status e identificá-la no portal de ajustes é ${idPagamento}`
                },
                StatusPagamento: {
                    _text: 'EnviadoAoValidador'
                }
            }
        }
    })

    return {
        filename,
        xml
    }
}
