/*
Exemplo de Saída

<?xml version="1.0" encoding="utf-8"?>
<Integrador>
	<Identificador>
		<Valor>30025</Valor>
	</Identificador>
	<IntegradorResposta>
		<Codigo>AP</Codigo>
		<Valor>Arquivo processado</Valor>
	</IntegradorResposta>
	<Resposta>
		<retorno>1234|06000|0000|Enviado com sucesso + Retorno SEFAZ.|||PG5mZURhZG9zTXNnIHhtbG5zPSJodHRwOi8vd3d3LnBvcnRhbGZpc2NhbC5pbmYuYnIvbmZlL3dzZGwvTmZlU3RhdHVzU2VydmljbzIiPg0KCQkJPGNvbnNTdGF0U2VydiB4bWxucz0iaHR0cDovL3d3dy5wb3J0YWxmaXNjYWwuaW5mLmJyL25mZSIgdmVyc2FvPSIzLjEwIj4NCgkJCQk8dHBBbWI+MjwvdHBBbWI+DQoJCQkJPGNVRj4yMzwvY1VGPg0KCQkJCTx4U2Vydj5TVEFUVVM8L3hTZXJ2Pg0KCQkJPC9jb25zU3RhdFNlcnY+DQogICAgICAgICAgIDwvbmZlRGFkb3NNc2c+|20110101170101|CFe35111202767579000148598583801050151865833992|65.53|12345678912|SiXo47hPq8asxsIvY2TqRUhI0mFKL+PVjNcGu/ws+OEOYvdGdlse87MlhjS60huafx+zplSgdxyZClxODqzIRyW30JJHLTX2Hu</retorno>
	</Resposta>
</Integrador>
*/

const debug = require('debug')
const wrapper = require('../js2xml-wrapper')
const { gerarDadosConsultaCFe, gerarChaveConsultaCFe } = require('../utils/gerar-chave-consulta')
const parseParametrosEntrada = require('../parse-params-entrada')
const NumeroCupom = require('../models/NumeroCupom')
const toCFeTimestamp = require('../utils/to-cfe-timestamp')
const cfeXmlStub = require('../../stub/mfe/cfe-xml-stub')

const d = debug('smf:MF-e EnviarDadosVenda')
const CODIGO_RETORNO_SUCESSO = '06000'
const MENSAGEM_SUCESSO = 'Emitido com sucesso + conteúdo notas.'

const currentDate = new Date()

/**
 *  Gera arquivo de Resposta para Método EnviarDadosVenda do componente MF-e
 *  @param  {Object}  entrada Objeto convertido de xml de entrada
 *  @param  {Object}  chance  Instância de chance, para gerar valores randômicos
 *  @return {Promise}         filename e xml de saída
 */
module.exports = async (entrada, chance) => {
    const params = parseParametrosEntrada(entrada)
    const filename = chance.word({ syllables: 5 }) + '.xml'
    const identificador = entrada.Integrador.Identificador.Valor._text
    d(`Gerando arquivo de saída para Identificador ${identificador}`)

    const { infCFe } = params.DadosVenda.CFe
    const { dest } = infCFe
    let cpfCnpjDest = ''
    if (dest.CPF) {
        cpfCnpjDest = dest.CPF._text
    } else if (dest.CNPJ) {
        cpfCnpjDest = dest.CNPJ._text
    }

    const model = await new NumeroCupom(null).save()
    const numero_cupom = model.id
    const dadosConsulta = gerarDadosConsultaCFe(chance, numero_cupom)
    const chaveConsulta = gerarChaveConsultaCFe(dadosConsulta)
    const arquivoCFeBase64 = Buffer.from(cfeXmlStub(chaveConsulta)).toString('base64')

    const retorno = [
        params.NumeroSessao,                // numeroSessao
        CODIGO_RETORNO_SUCESSO,             // Código de retorno
        '0000',                             // Código de retorno de cancelamento
        MENSAGEM_SUCESSO,                   // mensagem
        '',                                 // Código de referência
        '',                                 // mensagemSEFAZ
        arquivoCFeBase64,                   // arquivoCFeBase64
        toCFeTimestamp(currentDate),        // timeStamp
        chaveConsulta,              		// chaveConsulta
        chance.floating({ fixed: 2, min: 10, max: 1000 }), // valorTotalCFe
        cpfCnpjDest,                        // CPFCNPJValue
        Buffer.from(chaveConsulta).toString('base64') // assinaturaQRCODE
    ].join('|')

    const xml = wrapper({
        Integrador: {
            Identificador: {
                Valor: {
                    _text: identificador
                }
            },
            IntegradorResposta: {
                Codigo: {
                    _text: 'AP'
                },
                Valor: {
                    _text: 'Arquivo processado'
                }
            },
            Resposta: {
                retorno: {
                    _text: retorno
                }
            }
        }
    })

    return {
        filename,
        xml
    }
}
