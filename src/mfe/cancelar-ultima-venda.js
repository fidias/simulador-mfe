
const debug = require('debug')
const wrapper = rootRequire('src/js2xml-wrapper')
const { gerarDadosConsultaCFe, gerarChaveConsultaCFe } = require('../utils/gerar-chave-consulta')
const parseParametrosEntrada = rootRequire('src/parse-params-entrada')
const NumeroCupom = require('../models/NumeroCupom')
const toCFeTimestamp = rootRequire('src/utils/to-cfe-timestamp')
const arquivoCFeBase64 = rootRequire('src/utils/exemplo-retorno-base64')

const d = debug('smf:MF-e CancelarUltimaVenda')
const CODIGO_RETORNO_SUCESSO = '07000'
const MENSAGEM_SUCESSO = 'Cupom cancelado com sucesso + conteúdo CF-e-SAT cancelado.'

const currentDate = new Date()

/**
 *  Gera arquivo de Resposta para Método CancelarUltimaVenda do componente MF-e
 *  @param  {Object}  entrada Objeto convertido de xml de entrada
 *  @param  {Object}  chance  Instância de chance, para gerar valores randômicos
 *  @return {Promise}         Objeto com filename e xml de saída
 */
module.exports = async (entrada, chance) => {
    const params = parseParametrosEntrada(entrada)
    const filename = chance.word({ syllables: 5 }) + '.xml'
    const identificador = entrada.Integrador.Identificador.Valor._text
    d(`Gerando arquivo de saída para Identificador ${identificador}`)

    const { infCFe } = params.DadosCancelamento.CFeCanc
    const { dest } = infCFe
    let cpfCnpjDest = ''
    if (dest.CPF) {
        cpfCnpjDest = dest.CPF
    } else if (dest.CNPJ) {
        cpfCnpjDest = dest.CNPJ
    }

    const model = await new NumeroCupom(null).save()
    const numero_cupom = model.id
    const dadosConsulta = gerarDadosConsultaCFe(chance, numero_cupom)
    const chaveConsulta = gerarChaveConsultaCFe(dadosConsulta)

    const retorno = [
        params.NumeroSessao,                // numeroSessao
        CODIGO_RETORNO_SUCESSO,             // Código de retorno
        '0000',                             // Código de retorno de cancelamento
        MENSAGEM_SUCESSO,                   // mensagem
        '',                                 // Código de referência
        '',                                 // mensagemSEFAZ
        arquivoCFeBase64,                   // arquivoCFeBase64
        toCFeTimestamp(currentDate),        // timeStamp
        chaveConsulta,                     // chaveConsulta
        chance.floating({ fixed: 2, min: 10, max: 1000 }), // valorTotalCFe
        cpfCnpjDest,                        // CPFCNPJValue
        Buffer.from(chaveConsulta).toString('base64') // assinaturaQRCODE
    ].join('|')

    const xml = wrapper({
        Integrador: {
            Identificador: {
                Valor: {
                    _text: identificador
                }
            },
            IntegradorResposta: {
                Codigo: {
                    _text: 'AP'
                },
                Valor: {
                    _text: 'Arquivo processado'
                }
            },
            Resposta: {
                retorno: {
                    _text: retorno
                }
            }
        }
    })

    return {
        filename,
        xml
    }
}
