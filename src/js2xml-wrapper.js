
const convert = require('xml-js')
const _declaration = require('./xml-declaration')

module.exports = (object) => {
    return convert.js2xml({
        ..._declaration,
        ...object
    }, { compact: true })
}
