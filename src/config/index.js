const fs = require('fs')
const path = require('path')
const delve = require('dlv')
const isFunction = require('lodash.isfunction')
const file = path.resolve(process.cwd(), '.simulador-mfe.json')
let config = {}

function exists (file) {
    try {
        fs.statSync(file);
        return true;
    } catch (err) {
        if (err.code === 'ENOENT') {
            return false;
        }
    }
};

function readConfig () {
    if (!exists(file)) {
        fs.writeFileSync(file, '{}')
    }
    config = JSON.parse(fs.readFileSync(file))
}

readConfig()

function has (key) {
    return delve(config, key) !== undefined
}

/**
 * @param key {String}
 * @param defaultValue {Function | Any}
 */
function get (key, defaultValue) {
    return delve(config, key, isFunction(defaultValue) ? defaultValue() : defaultValue)
}

module.exports = {
    has,
    get,
    file
}
