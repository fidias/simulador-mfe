
const { orm } = require('../database')

module.exports = orm.Model.extend({
    tableName: 'vfpe_verificar_status_validador',
    idAttribute: 'id',
    hasTimestamps: true
})
