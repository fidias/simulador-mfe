const { orm } = require('../database')

module.exports = orm.Model.extend({
    tableName: 'numero_cupom',
    idAttribute: 'id',
    hasTimestamps: true
})
