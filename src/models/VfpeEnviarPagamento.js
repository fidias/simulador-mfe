
const { orm } = require('../database')

module.exports = orm.Model.extend({
    tableName: 'vfpe_enviar_pagamento',
    idAttribute: 'id',
    hasTimestamps: true
})
