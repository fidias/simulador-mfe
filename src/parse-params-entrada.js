
const convert = require('xml-js')
const optionsEntrada = require('./options-entrada')

module.exports = (entrada) => {
    const { Parametro } = entrada.Integrador.Componente.Metodo.Parametros
    // console.log('item', Parametro)
    return Parametro.reduce((obj, item) => {
        if (item.Valor._text) {
            if (item.Valor._text.toString().startsWith('<![CDATA[')) {
                let xml = item.Valor._text.toString()
                        .replace('<![CDATA[', '')
                        .replace(']]>', '')
                obj[item.Nome._text] = convert.xml2js(xml, optionsEntrada)
            } else {
                obj[item.Nome._text] = item.Valor._text
            }
        } else if (item.Valor._cdata) {
            obj[item.Nome._text] = convert.xml2js(item.Valor._cdata, optionsEntrada)
        }
        return obj
    }, {})
}
