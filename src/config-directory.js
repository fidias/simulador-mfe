const path = require('path')
const os = require('os')
const mkdirp = require('mkdirp')

const getDirFor = (appName, platform) => {
    switch (platform || process.platform) {
        case 'win32':
            if (process.env.APPDATA) {
                return path.join(process.env.APPDATA, appName)
            }
            return path.join(os.homedir(), 'AppData', 'Roaming', appName)
        case 'darwin':
            return path.join(os.homedir(), 'Library', 'Application Support', appName)
        default: // 'freebsd', 'linux', 'sunos' or other.
            if (process.env.XDG_CONFIG_HOME) {
                return path.join(process.env.XDG_CONFIG_HOME, appName)
            }
            return path.join(os.homedir(), '.config', appName)
    }
}

const getFileFor = (appName, file, platform) => {
    return path.join(getDirFor(appName, platform), file)
}

const create = (appName) => {
	return mkdirp.sync(getDirFor(appName));
}

module.exports = {
    getDirFor,
    getFileFor,
    create
}
