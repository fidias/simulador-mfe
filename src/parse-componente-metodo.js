
const metodos = {
    'VFP-e': {
        'EnviarPagamento': require('./vfpe/enviar-pagamento'),
        'VerificarStatusValidador': require('./vfpe/verificar-status-validador'),
        'RespostaFiscal': require('./vfpe/resposta-fiscal')
    },
    'MF-e': {
        'EnviarDadosVenda': require('./mfe/enviar-dados-venda'),
        'CancelarUltimaVenda': require('./mfe/cancelar-ultima-venda')
    },
    'NFCE': {
        'HNfeAutorizacaoLote12': require('./nfce/autorizacao-lote'),
        'HRecepcaoEvento': require('./nfce/recepcao-evento-cancelar'),
        'HNfeConsulta2Soap12': require('./nfce/consultar-situacao')
    }
}

/**
 *  Decide a partir da entrada qual arquivo de saída gerar
 *  @type {Object} Objeto convertido de xml de entrada
 *  @return {Function} Função específica para gerar saída a partir da entrada informada,
 *  ou null caso contrário
 *  @throws {Error} Quando método especificado não for encontrado
 */
module.exports = (entrada) => {
    const componente = entrada.Integrador.Componente._attributes.Nome
    const metodo = entrada.Integrador.Componente.Metodo._attributes.Nome
    if (metodos[componente]) {
        if (metodos[componente][metodo]) {
            return metodos[componente][metodo]
        }
    }
    throw new Error(`Não foi possível encontrar parser para ${componente}:${metodo}`)
}
