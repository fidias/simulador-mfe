
let knexOptions
if (process.env.NODE_ENV === 'test') {
    knexOptions = {
        client: 'sqlite3',
        connection: {
          filename: ':memory:'
        },
        useNullAsDefault: true
    }
} else {
    const configdir = require('./config-directory')
    knexOptions = {
        client: 'sqlite3',
        connection: {
          filename: configdir.getFileFor('simulador-mfe', 'database.sqlite3')
        },
        useNullAsDefault: true
    }
}

const knex = require('knex')(knexOptions)

module.exports = {
    config: knex,
    orm: require('bookshelf')(knex)
}
