/*
Exemplo de Saída

<?xml version="1.0" encoding="utf-8"?>
<Integrador>
  <Identificador>
    <Valor>015NLkA2EU</Valor>
  </Identificador>
  <IntegradorResposta>
    <Codigo>AP</Codigo>
    <Valor>Arquivo processado</Valor>
  </IntegradorResposta>
  <Resposta>
    <retorno>\"174752|06000|0000|Enviado com sucesso + Retorno SEFAZ-CE.|||PHNvYXA6RW52ZWxvcGUgeG1sbnM6c29hcD0iaHR0cDovL3d3dy53My5vcmcvMjAwMy8wNS9zb2FwLWVudmVsb3BlIj48ZW52OkhlYWRlciB4bWxuczplbnY9Imh0dHA6Ly93d3cudzMub3JnLzIwMDMvMDUvc29hcC1lbnZlbG9wZSIvPjxzb2FwOkJvZHk+PG5mZVJlc3VsdE1zZyB4bWxucz0iaHR0cDovL3d3dy5wb3J0YWxmaXNjYWwuaW5mLmJyL25mZS93c2RsL05GZUF1dG9yaXphY2FvNCI+PHJldEVudmlORmUgeG1sbnM9Imh0dHA6Ly93d3cucG9ydGFsZmlzY2FsLmluZi5ici9uZmUiIHZlcnNhbz0iNC4wMCI+PHRwQW1iPjE8L3RwQW1iPjx2ZXJBcGxpYz5DRV9ORkNlXzIuMi4xPC92ZXJBcGxpYz48Y1N0YXQ+MTA0PC9jU3RhdD48eE1vdGl2bz5Mb3RlIHByb2Nlc3NhZG88L3hNb3Rpdm8+PGNVRj4yMzwvY1VGPjxkaFJlY2J0bz4yMDE5LTA4LTI3VDExOjEwOjM4LTAzOjAwPC9kaFJlY2J0bz48cHJvdE5GZSB2ZXJzYW89IjQuMDAiPjxpbmZQcm90IElkPSJJRDEyMzE5MDAwMzQzMzMyNiI+PHRwQW1iPjE8L3RwQW1iPjx2ZXJBcGxpYz5DRV9ORkNlXzIuMi4xPC92ZXJBcGxpYz48Y2hORmU+MjMxOTA4MTQ1Mzg4NDUwMDAxNDA2NTAwMTAwMDAwMDAwMTE2Njg4OTMzNTg8L2NoTkZlPjxkaFJlY2J0bz4yMDE5LTA4LTI3VDExOjEwOjM4LTAzOjAwPC9kaFJlY2J0bz48blByb3Q+MTIzMTkwMDAzNDMzMzI2PC9uUHJvdD48ZGlnVmFsPmN0M1BJOThqdU5iN0NpMktzZGx0UC9uR2pnUT08L2RpZ1ZhbD48Y1N0YXQ+MTAwPC9jU3RhdD48eE1vdGl2bz5BdXRvcml6YWRvIG8gdXNvIGRhIE5GLWU8L3hNb3Rpdm8+PC9pbmZQcm90PjwvcHJvdE5GZT48L3JldEVudmlORmU+PC9uZmVSZXN1bHRNc2c+PC9zb2FwOkJvZHk+PC9zb2FwOkVudmVsb3BlPg==|</retorno>
  </Resposta>
</Integrador>

*/

const convert = require('xml-js')
const optionsEntrada = require('../options-entrada')
const debug = require('debug')
const wrapper = require('../js2xml-wrapper')
const parseParametrosEntrada = require('../parse-params-entrada')
const stub = require('../../stub/nfce/recepcao-evento-cancelar-stub')

const d = debug('smf:NFC-e RecepcaoEvento Cancelar')
const CODIGO_RETORNO_SUCESSO = '06000'
const MENSAGEM_SUCESSO = 'Enviado com sucesso + Retorno SEFAZ-CE.'

module.exports = async (entrada, chance) => {
    const params = parseParametrosEntrada(entrada)
    const filename = chance.word({ syllables: 5 }) + '.xml'
    const identificador = entrada.Integrador.Identificador.Valor._text
    d(`Gerando arquivo de saída para Identificador ${identificador}`)

    var dados = Buffer.from(params.dados, 'base64').toString('ascii');
    const soap = convert.xml2js(dados, {...optionsEntrada, nativeType: false})
    const infEvento = soap['soap12:Envelope']['soap12:Body'].nfeDadosMsg.envEvento.evento.infEvento
    // d(infEvento)
    const chaveConsulta = infEvento.chNFe._text

    const arquivoBase64 = Buffer.from(stub(chance, chaveConsulta)).toString('base64')

    const retorno = [
        '\\"' + params.numeroSessao,        // numeroSessao
        CODIGO_RETORNO_SUCESSO,             // Código de retorno
        '0000',                             // Código de retorno de cancelamento
        MENSAGEM_SUCESSO,                   // mensagem
        '',                                 // Código de referência
        '',                                 // mensagemSEFAZ
        arquivoBase64,                      // arquivoNFCeBase64
        ''
    ].join('|')

    const xml = wrapper({
        Integrador: {
            Identificador: {
                Valor: {
                    _text: identificador
                }
            },
            IntegradorResposta: {
                Codigo: {
                    _text: 'AP'
                },
                Valor: {
                    _text: 'Arquivo processado'
                }
            },
            Resposta: {
                retorno: {
                    _text: retorno
                }
            }
        }
    })

    return {
        filename,
        xml
    }
}
