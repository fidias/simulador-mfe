
const zeroPad2 = require('./zero-pad-2')

/**
 *  Converte um Date para o formato exigido pelo MF-e
 *  @param  {Date} date
 *  @return {String}
 */
module.exports = date => ([
    date.getFullYear(),
    zeroPad2(date.getMonth() + 1),
    zeroPad2(date.getDate()),
    zeroPad2(date.getHours()),
    zeroPad2(date.getMinutes()),
    zeroPad2(date.getSeconds())
].join(''))
