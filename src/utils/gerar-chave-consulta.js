const dayjs = require('dayjs')
const padStart = require('lodash.padstart')
const config = require('../config')

const MOD_CFE = 59

function gerarDadosConsultaCFe (chance, numero_cupom) {
    const uf = config.get('emitente.c_uf', () => (
        chance.integer({ min: 10, max: 99 })
    ))
    const cnpj = config.get('emitente.cnpj', () => (
        chance.cnpj().replace(/\D/g, '')
    ))
    const numero_serie = config.get('modulo_fiscal.numero_serie', () => (
        parseInt(chance.string({ pool: '123456789', length: 9 }))
    ))

    const date = dayjs()

    return {
        uf,
        cnpj,
        numero_serie,
        codigo_numerico: parseInt(chance.string({ pool: '123456789', length: 6 })),
        ano: date.format('YY'),
        mes: date.format('MM'),
        numero_cupom
    }
}

function gerarChaveConsultaCFe (dados = {}) {
    const { uf, ano, mes, cnpj, numero_serie, numero_cupom, codigo_numerico } = dados
    const chave = `${uf}${ano}${mes}${cnpj}${MOD_CFE}${numero_serie}${padStart(numero_cupom, 6, '0')}${codigo_numerico}`
    return `CFe${chave}${modulo11(chave)}`
}

module.exports = {
    gerarDadosConsultaCFe,
    gerarChaveConsultaCFe
}

function modulo11 (chave) {
    let total = 0, peso = 2
    for (let i = chave.length - 1; i > -1; i--) {
        const charAt = chave[i]
        total += parseInt(charAt) * peso
        peso++
        if (peso === 10) {
            peso = 2
        }
    }
    const resto = total % 11
    return resto === 0 || resto === 1 ? 0 : 11 - resto
}
