/**
 * Retorna número de 15 dígitos, não iniciado por zero
 * @param {type} chance
 * @returns {String}
 */
module.exports = (chance) => {
    return `${chance.string({length: 1, pool: '123456789'})}${chance.string({length: 14, pool: '0123456789'})}`
}
