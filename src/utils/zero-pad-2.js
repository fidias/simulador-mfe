
const padStart = require('lodash.padstart')

/**
 *  Adiciona até 2 zeros a esquerda de um input
 *  @param  {any} input
 *  @return {String}
 */
module.exports = input => padStart(input, 2, '0')
