/**
 *  Based on https://gist.github.com/branneman/8048520#7-the-wrapper
 */
global.rootRequire = function(name) {
    return require(__dirname + '/' + name);
}
