#!/usr/bin/env node

'use strict'

require('./root-require')

const meow = require('meow')
const parsePath = require('parse-filepath')
const chokidar = require('chokidar')
const debug = require('debug')
const convert = require('xml-js')
const fs = require('fs')
const path = require('path')
const Chance = require('chance')

const configdir = require('./src/config-directory')
const db = require('./src/database')
const parseMetodo = require('./src/parse-componente-metodo')
const optionsEntrada = require('./src/options-entrada')
const chance = new Chance()
const dsim = debug('smf:CLI')

const cli = meow(`
  Uso
    $ simulador-mfe --dir-entrada <dir-entrada> --dir-saida <dir-saida>
    $ simulador-mfe -e <dir-entrada> -s <dir-saida>

  Opções
    --dir-entrada, -e   Diretório para simular entrada de arquivos
    --dir-saida, -s     Diretório para simular saída de arquivos
    --help              Imprime esta ajuda
    --debug             Habilita modo debug (Todos os logs)
    --verbose           Habilita modo verbose (logs do Simulador)
    --version           Imprime versão
`, {
    flags: {
        dirEntrada: {
            type: 'string',
            alias: 'e',
            default: ''
        },
        dirSaida: {
            type: 'string',
            alias: 's',
            default: ''
        },
        debug: {
            type: 'boolean',
            default: false
        },
        verbose: {
            type: 'boolean',
            default: false
        }
    }
})

const isEmpty = value => value === undefined || value === null || value.length === 0

if (isEmpty(cli.flags.dirEntrada) || isEmpty(cli.flags.dirSaida)) {
    cli.showHelp()
    process.exit()
}

const dirEntrada = parsePath(cli.flags.dirEntrada)
const dirSaida = parsePath(cli.flags.dirSaida)

if (cli.flags.debug) {
    debug.enable('*')
} else if (cli.flags.verbose) {
    debug.enable('smf:*')
}

const watcherE = chokidar.watch(dirEntrada.absolute, {
    ignored: /(^|[\/\\])\../,
    persistent: true,
    depth: 1,
    awaitWriteFinish: {
        stabilityThreshold: 1000
    }
})

const watcherS = chokidar.watch(dirSaida.absolute, {
    ignored: /(^|[\/\\])\../,
    persistent: true,
    depth: 1
})

configdir.create('simulador-mfe')

db.config.migrate.latest()
    .then(() => {
        watcherE
        .on('add', arquivo => {
            dsim(`Arquivo \`${arquivo}\` foi adicionado a entrada`)
            fs.readFile(arquivo, async (err, data) => {
                if (err) throw err

                dsim(`Fazendo parse do arquivo de entrada \`${arquivo}\` ...`)
                const result = convert.xml2js(data, optionsEntrada)
                try {
                    const metodo = parseMetodo(result)
                    const saida = await metodo(result, chance)
                    dsim(`Gerando arquivo de saída \`${saida.filename}\` ...`)
                    fs.writeFileSync(path.join(dirSaida.absolute, saida.filename), saida.xml)

                    fs.unlinkSync(arquivo)
                } catch (ex) {
                    dsim(`Erro ao fazer parse do arquivo de entrada \`${arquivo}\`.`)
                    console.error(ex.message)
                }
            })
        })
        .on('unlink', arquivo => {
            dsim(`Arquivo de entrada \`${arquivo}\` foi lido e removido`)
        })

        watcherS
        .on('add', arquivo => {
            dsim(`Arquivo de saída \`${arquivo}\` gerado com sucesso`)
        })
        .on('unlink', arquivo => {
            dsim(`Arquivo de saída \`${arquivo}\` removido pelo usuário`)
        })
    })

const gracefulShutdown = () => {
    console.log('Received kill signal, shutting down gracefully.')
    watcherE.close()
    watcherS.close()
    process.exit()
}

// listen for TERM signal .e.g. kill
process.on('SIGTERM', gracefulShutdown)

// listen for INT signal e.g. Ctrl-C
process.on('SIGINT', gracefulShutdown)
