
const test = require('ava')
const Chance = require('chance')
const { gerarDadosConsultaCFe, gerarChaveConsultaCFe } = rootRequire('src/utils/gerar-chave-consulta')
const NumeroCupom = rootRequire('src/models/NumeroCupom')
const db = rootRequire('src/database')

const chance = new Chance()

test.before(async t => {
    await db.config.migrate.latest()
})

test('Gerar dados para geração da Chave de Consulta CF-e', async t => {
    const model = await new NumeroCupom(null).save()
    const numero_cupom = model.id
    const dados = gerarDadosConsultaCFe(chance, numero_cupom)

    t.true(dados.uf > 0)
    t.true(dados.cnpj.toString().length === 14)
    t.regex(dados.numero_serie.toString(), /^\d{9}$/)
    t.regex(dados.ano.toString(), /^\d{2}$/)
    t.regex(dados.mes.toString(), /^\d{2}$/)

    const chaveConsulta = gerarChaveConsultaCFe(dados)
    t.regex(chaveConsulta, /^\d{44}$/)
})
