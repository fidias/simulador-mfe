
const test = require('ava')
const Chance = require('chance')
const convert = require('xml-js')
const optionsEntrada = require('../../src/options-entrada')

const db = require('../../src/database')
const EnviarPagamento = require('../../src/vfpe/enviar-pagamento')
const stubEnviarPagamento = require('../../stub/vfpe/enviar-pagamento-stub')
const VerificarStatusValidador = require('../../src/vfpe/verificar-status-validador')
const stubVerificarStatusValidador = require('../../stub/vfpe/verificar-status-validador-stub')

test.before(async t => {
    await db.config.migrate.latest()
})

test('Enviar entrada VFP-e VerificarStatusValidador', async t => {
    const chance = new Chance()

    // depende de EnviarPagamento
    const entradaPag = stubEnviarPagamento(chance)
    const saidaPag = await EnviarPagamento(entradaPag, chance)
    const saidaPagResult = convert.xml2js(saidaPag.xml, optionsEntrada)

    const idFila = saidaPagResult.Integrador.Resposta.IdPagamento._text
    const entrada = stubVerificarStatusValidador(chance, idFila)
    const saida = await VerificarStatusValidador(entrada, chance)
    const identificador = entrada.Integrador.Identificador.Valor._text

    t.truthy(saida instanceof Object)
    t.truthy(typeof saida.filename === 'string')
    t.truthy(saida.filename.includes('.xml'))
    t.truthy(typeof saida.xml === 'string')
    t.truthy(saida.xml.includes(`<Identificador><Valor>${identificador}</Valor></Identificador>`))
})
