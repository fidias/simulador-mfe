
const test = require('ava')
const Chance = require('chance')

const db = require('../../src/database')
const RespostaFiscal = require('../../src/vfpe/resposta-fiscal')
const stubRespostaFiscal = require('../../stub/vfpe/resposta-fiscal-stub')

test.before(async t => {
    await db.config.migrate.latest()
})

test('Enviar entrada VFP-e RespostaFiscal', async t => {
    const chance = new Chance()
    const entrada = stubRespostaFiscal(chance)
    const saida = await RespostaFiscal(entrada, chance)
    const identificador = entrada.Integrador.Identificador.Valor._text

    t.truthy(saida instanceof Object)
    t.truthy(typeof saida.filename === 'string')
    t.truthy(saida.filename.includes('.xml'))
    t.truthy(typeof saida.xml === 'string')
    t.truthy(saida.xml.includes(`<Identificador><Valor>${identificador}</Valor></Identificador>`))
})
