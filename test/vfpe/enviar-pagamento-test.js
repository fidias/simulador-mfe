
const test = require('ava')
const Chance = require('chance')

const db = require('../../src/database')
const EnviarPagamento = require('../../src/vfpe/enviar-pagamento')
const stubEnviarPagamento = require('../../stub/vfpe/enviar-pagamento-stub')

test.before(async t => {
    await db.config.migrate.latest()
})

test('Enviar entrada VFP-e EnviarPagamento', async t => {
    const chance = new Chance()
    const entrada = stubEnviarPagamento(chance)
    const saida = await EnviarPagamento(entrada, chance)
    const identificador = entrada.Integrador.Identificador.Valor._text

    t.truthy(saida instanceof Object)
    t.truthy(typeof saida.filename === 'string')
    t.truthy(saida.filename.includes('.xml'))
    t.truthy(typeof saida.xml === 'string')
    t.truthy(saida.xml.includes(`<Identificador><Valor>${identificador}</Valor></Identificador>`))
})
