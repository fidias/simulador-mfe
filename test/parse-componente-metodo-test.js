
const test = require('ava')
const parseMetodo = rootRequire('src/parse-componente-metodo')

test('Obter parser através dos dados de Entrada XML', t => {
    const entrada = {
        Integrador: {
            Componente: {
                _attributes: {
                    Nome: 'MF-e'
                },
                Metodo: {
                    _attributes: {
                        Nome: 'EnviarDadosVenda'
                    }
                }
            }
        }
    }

    t.truthy(parseMetodo(entrada) instanceof Function)
})

test('Exceção deve ser obtida caso dados de Entrada não sejam conhecidos', t => {
    const entrada = {
        Integrador: {
            Componente: {
                _attributes: {
                    Nome: 'Teste'
                },
                Metodo: {
                    _attributes: {
                        Nome: 'Fake'
                    }
                }
            }
        }
    }

    const err = t.throws(() => {
        parseMetodo(entrada)
    }, { instanceOf: Error })

    t.true(err.message.startsWith('Não foi possível encontrar parser'))
})
