
const test = require('ava')
const Chance = require('chance')
const stubCancelarUltimaVenda = rootRequire('stub/mfe/cancelar-ultima-venda-stub')
const CancelarUltimaVenda = rootRequire('src/mfe/cancelar-ultima-venda')

test('Enviar entrada MF-e CancelarUltimaVenda', async t => {
    const chance = new Chance()

    const entrada = stubCancelarUltimaVenda(chance)
    const saida = await CancelarUltimaVenda(entrada, chance)
    const identificador = entrada.Integrador.Identificador.Valor._text

    t.truthy(saida instanceof Object)
    t.truthy(typeof saida.filename === 'string')
    t.truthy(saida.filename.includes('.xml'))
    t.truthy(typeof saida.xml === 'string')
    t.truthy(saida.xml.includes(`<Identificador><Valor>${identificador}</Valor></Identificador>`))
})
