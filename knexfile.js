
const configdir = require('./src/config-directory')

configdir.create('simulador-mfe')

module.exports = {
    client: 'sqlite3',
    connection: {
        filename: configdir.getFileFor('simulador-mfe', 'database.sqlite3')
    }
}
