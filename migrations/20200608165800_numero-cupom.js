
exports.up = function (knex) {
    return knex.schema
        .createTable('numero_cupom', table => {
            table.increments('id').unsigned().primary()
            table.timestamps()
        })
}

exports.down = function (knex) {
    return knex.schema
        .dropTable('numero_cupom')
}
