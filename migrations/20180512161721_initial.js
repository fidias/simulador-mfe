
exports.up = function (knex) {
    return knex.schema
        .createTable('vfpe_enviar_pagamento', table => {
            table.increments('id').unsigned().primary()
            table.string('identificador').notNull()
            table.string('serial_pos').notNull()
            table.decimal('valor_venda').notNull()
            table.string('id_fila').notNull()
            table.timestamps()
        })
        .createTable('vfpe_verificar_status_validador', table => {
            table.increments('id').unsigned().primary()
            table.string('identificador').notNull()
            table.string('id_fila').notNull()
            table.string('codigo_autorizacao').notNull()
            table.string('codigo_pagamento').notNull()
            table.string('tipo_bandeira').notNull()
            table.timestamps()
        })
}

exports.down = function (knex) {
    return knex.schema
        .dropTable('vfpe_enviar_pagamento')
        .dropTable('vfpe_verificar_status_validador')
}
