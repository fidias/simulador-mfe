const { normalizeWhitespaces } = require('normalize-text')
const rfc3339 = require('../../src/utils/rfc3339')
const numeroProtocolo = require('../../src/utils/numero-protocolo')

module.exports = (chance, chaveConsulta) => {
    const currentDate = new Date()
    const nProt = numeroProtocolo(chance)
    return normalizeWhitespaces(`<soap:Envelope
	xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
	<soap:Header>
        <nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento">
            <cUF>23</cUF>
            <versaoDados>1.00</versaoDados>
        </nfeCabecMsg>
    </soap:Header>
    <soap:Body>
        <nfeRecepcaoEventoResult xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento">
            <retEnvEvento
                xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.00">
                <idLote>1</idLote>
                <tpAmb>2</tpAmb>
                <verAplic>CE_NFCe_2.2.1</verAplic>
                <cOrgao>23</cOrgao>
                <cStat>128</cStat>
                <xMotivo>Lote de Evento Processado</xMotivo>
                <retEvento versao="1.00">
                    <infEvento>
                        <tpAmb>2</tpAmb>
                        <verAplic>CE_NFCe_2.2.1</verAplic>
                        <cOrgao>23</cOrgao>
                        <cStat>135</cStat>
                        <xMotivo>Evento registrado e vinculado a NF-e</xMotivo>
                        <chNFe>${chaveConsulta}</chNFe>
                        <tpEvento>110111</tpEvento>
                        <xEvento>Cancelamento homologado</xEvento>
                        <nSeqEvento>1</nSeqEvento>
                        <CNPJDest>13279313000172</CNPJDest>
                        <dhRegEvento>${rfc3339(currentDate)}</dhRegEvento>
                        <nProt>${nProt}</nProt>
                    </infEvento>
                </retEvento>
            </retEnvEvento>
        </nfeRecepcaoEventoResult>
    </soap:Body>
</soap:Envelope>`)
}
