const { normalizeWhitespaces } = require('normalize-text')
const rfc3339 = require('../../src/utils/rfc3339')
const numeroProtocolo = require('../../src/utils/numero-protocolo')

module.exports = (chance, chaveConsulta) => {
    const currentDate = new Date()
    const nProt = numeroProtocolo(chance)
    return normalizeWhitespaces(`<soap:Envelope
	xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
	<env:Header
		xmlns:env="http://www.w3.org/2003/05/soap-envelope"/>
    <soap:Body>
        <nfeResultMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeAutorizacao4">
            <retEnviNFe
                xmlns="http://www.portalfiscal.inf.br/nfe" versao="4.00">
                <tpAmb>2</tpAmb>
                <verAplic>CE_NFCe_2.2.1</verAplic>
                <cStat>104</cStat>
                <xMotivo>Lote processado</xMotivo>
                <cUF>23</cUF>
                <dhRecbto>${rfc3339(currentDate)}</dhRecbto>
                <protNFe versao="4.00">
                    <infProt Id="ID${nProt}">
                        <tpAmb>2</tpAmb>
                        <verAplic>CE_NFCe_2.2.1</verAplic>
                        <chNFe>${chaveConsulta}</chNFe>
                        <dhRecbto>${rfc3339(currentDate)}</dhRecbto>
                        <nProt>${nProt}</nProt>
                        <digVal>${Buffer.from(chaveConsulta).toString('base64')}</digVal>
                        <cStat>100</cStat>
                        <xMotivo>Autorizado o uso da NF-e</xMotivo>
                    </infProt>
                </protNFe>
            </retEnviNFe>
        </nfeResultMsg>
    </soap:Body>
</soap:Envelope>`)
}
