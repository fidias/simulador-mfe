
module.exports = (chance) => {
    return {
        Integrador: {
            Identificador: {
                Valor: {
                    _text: chance.string({ length: 13, pool: '0123456789'})
                }
            },
            Componente: {
                _attributes: {
                    Nome: 'VFP-e'
                },
                Metodo: {
                    _attributes: {
                        Nome: 'RespostaFiscal'
                    },
                    Construtor: {
                        Parametros: {
                            Parametro: [
                                {
                                    Nome: {
                                        _text: 'ChaveAcessoValidador'
                                    },
                                    Valor: {
                                        _text: chance.guid()
                                    }
                                }
                            ]
                        }
                    },
                    Parametros: {
                        Parametro: [
                            {
                                Nome: {
                                    _text: 'idFila'
                                },
                                Valor: {
                                    _text: chance.string({ length: 7, pool: '0123456789'})
                                }
                            },
                            {
                                Nome: {
                                    _text: 'ChaveAcesso'
                                },
                                Valor: {
                                    _text: `CFe${chance.string({ length: 44, pool: '0123456789' })}`
                                }
                            },
                            {
                                Nome: {
                                    _text: 'Nsu'
                                },
                                Valor: {
                                    _text: chance.string({ length: 7, pool: '0123456789'})
                                }
                            },
                            {
                                Nome: {
                                    _text: 'NumerodeAprovacao'
                                },
                                Valor: {
                                    _text: chance.string({ length: 7, pool: '0123456789'})
                                }
                            },
                            {
                                Nome: {
                                    _text: 'Bandeira'
                                },
                                Valor: {
                                    _text: chance.integer({ min: 1, max: 9 })
                                }
                            },
                            {
                                Nome: {
                                    _text: 'Adquirente'
                                },
                                Valor: {
                                    _text: chance.name()
                                }
                            },
                            {
                                Nome: {
                                    _text: 'CNPJ'
                                },
                                Valor: {
                                    _text: chance.cnpj()
                                }
                            },
                            {
                                Nome: {
                                    _text: 'ImpressaoFiscal'
                                },
                                Valor: {
                                    _text: ''
                                }
                            },
                            {
                                Nome: {
                                    _text: 'NumeroDocumento'
                                },
                                Valor: {
                                    _text: chance.string({ length: 7, pool: '0123456789'})
                                }
                            }
                        ]
                    }
                }
            }
        }
    }
}
