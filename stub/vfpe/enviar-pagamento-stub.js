
module.exports = (chance) => {
    return {
        Integrador: {
            Identificador: {
                Valor: {
                    _text: chance.string({ length: 13, pool: '0123456789'})
                }
            },
            Componente: {
                _attributes: {
                    Nome: 'VFP-e'
                },
                Metodo: {
                    _attributes: {
                        Nome: 'EnviarPagamento'
                    },
                    Construtor: {
                        Parametros: {
                            Parametro: [
                                {
                                    Nome: {
                                        _text: 'ChaveAcessoValidador'
                                    },
                                    Valor: {
                                        _text: chance.guid()
                                    }
                                }
                            ]
                        }
                    },
                    Parametros: {
                        Parametro: [
                            {
                                Nome: {
                                    _text: 'ChaveRequisicao'
                                },
                                Valor: {
                                    _text: chance.guid()
                                }
                            },
                            {
                                Nome: {
                                    _text: 'Estabelecimento'
                                },
                                Valor: {
                                    _text: chance.integer({min: 1, max: 10})
                                }
                            },
                            {
                                Nome: {
                                    _text: 'SerialPOS'
                                },
                                Valor: {
                                    _text: chance.string({ length: 8 })
                                }
                            },
                            {
                                Nome: {
                                    _text: 'CNPJ'
                                },
                                Valor: {
                                    _text: chance.cnpj()
                                }
                            },
                            {
                                Nome: {
                                    _text: 'IcmsBase'
                                },
                                Valor: {
                                    _text: 0.0
                                }
                            },
                            {
                                Nome: {
                                    _text: 'ValorTotalVenda'
                                },
                                Valor: {
                                    _text: '100,00'
                                }
                            },
                            {
                                Nome: {
                                    _text: 'HabilitarMultiplosPagamentos'
                                },
                                Valor: {
                                    _text: false
                                }
                            },
                            {
                                Nome: {
                                    _text: 'HabilitarControleAntiFraude'
                                },
                                Valor: {
                                    _text: true
                                }
                            },
                            {
                                Nome: {
                                    _text: 'CodigoMoeda'
                                },
                                Valor: {
                                    _text: 'BRL'
                                }
                            },
                            {
                                Nome: {
                                    _text: 'EmitirCupomNFCE'
                                },
                                Valor: {
                                    _text: false
                                }
                            },
                            {
                                Nome: {
                                    _text: 'OrigemPagamento'
                                },
                                Valor: {
                                    _text: chance.cc_type()
                                }
                            }
                        ]
                    }
                }
            }
        }
    }
}
