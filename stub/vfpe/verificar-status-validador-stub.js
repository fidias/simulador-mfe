
module.exports = (chance, idFila) => {
    return {
        Integrador: {
            Identificador: {
                Valor: {
                    _text: chance.string({ length: 13, pool: '0123456789'})
                }
            },
            Componente: {
                _attributes: {
                    Nome: 'VFP-e'
                },
                Metodo: {
                    _attributes: {
                        Nome: 'VerificarStatusValidador'
                    },
                    Construtor: {
                        Parametros: {
                            Parametro: [
                                {
                                    Nome: {
                                        _text: 'ChaveAcessoValidador'
                                    },
                                    Valor: {
                                        _text: chance.guid()
                                    }
                                }
                            ]
                        }
                    },
                    Parametros: {
                        Parametro: [
                            {
                                Nome: {
                                    _text: 'IdFila'
                                },
                                Valor: {
                                    _text: idFila
                                }
                            },
                            {
                                Nome: {
                                    _text: 'CNPJ'
                                },
                                Valor: {
                                    _text: chance.cnpj()
                                }
                            }
                        ]
                    }
                }
            }
        }
    }
}
