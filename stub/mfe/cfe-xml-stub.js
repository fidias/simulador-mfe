module.exports = chaveAcesso =>
  `<CFe>
        <infCFe Id="${chaveAcesso}" versao="0.07" versaoDadosEnt="0.07" versaoSB="010000">
            <ide>
                <nCFe>000036</nCFe>
                <dEmi>20180717</dEmi>
                <hEmi>092155</hEmi>
                <nserieSAT>900001234</nserieSAT>
            </ide>
        </infCFe>
    </CFe>`;
