
module.exports = chance => {
    const chave = `CFe${chance.string({ length: 44, pool: '0123456789'})}`
    const chaveCanc = `CFe${chance.string({ length: 44, pool: '0123456789'})}`

    return {
        Integrador: {
            Identificador: {
                Valor: {
                    _text: chance.string({ length: 13, pool: '0123456789'})
                }
            },
            Componente: {
                _attributes: {
                    Nome: 'MF-e'
                },
                Metodo: {
                    _attributes: {
                        Nome: 'CancelarUltimaVenda'
                    },
                    Parametros: {
                        Parametro: [
                            {
                                Nome: {
                                    _text: 'NumeroSessao'
                                },
                                Valor: {
                                    _text: chance.integer({min: 1, max: 100})
                                }
                            },
                            {
                                Nome: {
                                    _text: 'CodigoDeAtivacao'
                                },
                                Valor: {
                                    _text: chance.string({ length: 12 })
                                }
                            },
                            {
                                Nome: {
                                    _text: 'Chave'
                                },
                                Valor: {
                                    _text: chaveCanc
                                }
                            },
                            {
                                Nome: {
                                    _text: 'DadosCancelamento'
                                },
                                Valor: {
                                    _cdata: `<CFeCanc>
	<infCFe versao="0.07" Id="${chave}" chCanc="${chaveCanc}">
		<dEmi>20180531</dEmi>
		<hEmi>104049</hEmi>
		<ide>
			<cUF>23</cUF>
			<cNF>810186</cNF>
			<mod>59</mod>
			<nserieSAT>900013041</nserieSAT>
			<nCFe>007792</nCFe>
			<dEmi>20180531</dEmi>
			<hEmi>104049</hEmi>
			<cDV>9</cDV>
			<CNPJ>30146465000116</CNPJ>
			<signAC>MD2Nof/O0tQMPKiYeeAydSjYt7YV9kU0nWKZGXHVdYIzR2W9Z6tgXni/Y5bnjmUAk8MkqlBJIiOOIskKCjJ086k7vAP0EU5cBRYj/nzHUiRdu9AVD7WRfVs00BDyb5fsnnKg7gAXXH6SBgCxG9yjAkxJ0l2E2idsWBAJ5peQEBZqtHytRUC+FLaSfd3+66QNxIBlDwQIRzUGPaU6fvErVDSfMUf8WpkwnPz36fCQnyLypqe/5mbox9pt3RCbbXcYqnR/4poYGr9M9Kymj4/PyX9xGeiXwbgzOOHNIU5M/aAs0rulXz948bZla0eXABgEcp6mDkTzweLPZTbmOhX+eA==</signAC>
            <assinaturaQRCODE>HRilgAH2DGisl2vrocOwF0dQMKP4/xKPGW9yDcGdrmYNSNbZU9xpb7HhVKC6Kf7y8CxziAf6wSkKn2IpE+C23nib4TYoUj0mkXqIaLyMnNsbtsUx6dhtIPDH96F0MwFaVztf3B+lFIT6OaUgWDtcv8+szqZrXych/g7aN5Q2fhg0Qn+gKwDY05GnWD/2Kgvd628Q9efYMQjCXjMqvXc650D5f3d7Q28yGo80D9uWTfQFmKktzVRs7IjzUhBj7blIejfToQipWWC7ymM+afc6fYvQ1Bm1ZrovsPrqcTE2y9W6X8dxcFnbOHKQaFdVd+Ve8FV7cYFl9JvBQH9qOOlCIQ==</assinaturaQRCODE>
			<numeroCaixa>001</numeroCaixa>
		</ide>
		<emit>
			<CNPJ>30146465000116</CNPJ>
			<xNome>CF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome>
			<enderEmit>
				<xLgr>RUA 1</xLgr>
				<nro>100</nro>
				<xBairro>TESTE</xBairro>
				<xMun>MARACANAU</xMun>
				<CEP>61900000</CEP>
			</enderEmit>
			<IE>065911482</IE>
		</emit>
		<dest/>
		<total>
			<vCFe>100.00</vCFe>
		</total>
	</infCFe>
</CFeCanc>`
                                }
                            }
                        ]
                    }
                }
            }
        }
    }
}
